package com.suaoc.shopfinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

public class Layer2Fragment extends Fragment {

	Layer2Adapter adapter = null;
	CustomListView gv = null;
	ProgressBar pb = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.grid_layer2, container, false);
		pb = (ProgressBar) v.findViewById(R.id.layer1prg);
		return v;
	}

	// show layer 2
	public void start(boolean local, ArrayList<String> filters) {
		pb.setVisibility(View.VISIBLE);
		if (adapter != null) {
			adapter.layer2.clear();
			adapter.notifyDataSetChanged();
		}
		// start background task for layer 2
		Layer2Worker l2w = new Layer2Worker(this, local, filters);
		l2w.execute();
		boolean dontShow = ((MainActivity) getActivity()).getDontShow();
		if(!dontShow && !local) {
			// show info dialog if user has opted for it
			showInitDialog();
		}
	}

	// when data is recieved from server, show it in the list
	public void updateGridView(final List<Layer2Object> objs) {
		if(getActivity() == null || getActivity().isFinishing()) {
			return;
		}
		pb.setVisibility(View.GONE);
		if (objs == null || objs.size() <= 0) {
			showNoDataDialog();
			return;
		}
		gv = (CustomListView ) getView().findViewById(R.id.grid_view);
		gv.setFastScrollEnabled(true);
		adapter = new Layer2Adapter(getActivity(), objs);
		gv.setAdapter(adapter);
		gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				if(popup != null) {
					popup.dismiss();
					popup = null;
					return;
				}
				Layer2Object o = objs.get(position);
				((CustomTextView2) ((RelativeLayout) v).findViewById(R.id.name)).toggle(o.isSelected, o.curled);
				o.isSelected = !o.isSelected;
			}
		});
		gv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View v, int pos, long id) {
				showPopupWindow(v, objs.get(pos));
				return true;
			}
		});
		gv.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				if(popup != null) {
					popup.dismiss();
				}
			}
		});
	}
	
	// show info dialog for layer 2
	public void showInitDialog() {
		final Dialog d = new Dialog(getActivity());
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_layer2_init);
		d.setCancelable(false);
		final CheckBox checkBox = (CheckBox) d.findViewById(R.id.dontShow);
		Button ok = (Button) d.findViewById(R.id.btnok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				d.dismiss();
				if(checkBox.isChecked()) {
					// dont show again
					((MainActivity) getActivity()).setDontShow(true);
				}
			}
		});
		d.show();
	}
	
	// send selected data for email/print
	private void sendData(String text) {
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, text);
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
		sendIntent.putExtra(Intent.EXTRA_TITLE, "Title");
		sendIntent.setType("text/plain");
		startActivity(sendIntent);
	}

	// show popup 
	PopupWindow popup = null;
	private void showPopupWindow(View v, final Layer2Object o) {
		try {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View contentView = layoutInflater.inflate(R.layout.popup_layer2, null);

			if(popup != null) {
				popup.dismiss();
			}
			DisplayMetrics dm = getResources().getDisplayMetrics();
			int minw = (int)(dm.widthPixels / 320 * 170);
			contentView.setMinimumWidth(minw);
			(contentView.findViewById(R.id.higher)).setMinimumWidth(minw);
			popup = new PopupWindow(contentView, 300, 200);
			popup.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_popup));
			popup.setWindowLayoutMode(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			int sel = adapter.getSelectedCount();
			TextView warehouse = (TextView) popup.getContentView().findViewById(R.id.warehouse);
			warehouse.setText(sel == 0 ? "warehouse" : "warehouse" + " (" + sel + ")");
			popup.showAsDropDown(v);
//			log(v.getTop() + " : " + getResources().getDisplayMetrics().heightPixels);
			OnClickListener popupListener = new OnClickListener() {

				@Override
				public void onClick(View v) {
					popup.getContentView().findViewById(R.id.details).setBackgroundResource(R.drawable.bg_rect_padded_normal_2);
					popup.getContentView().findViewById(R.id.higher).setBackgroundResource(R.drawable.bg_rect_padded_normal_2);
					popup.getContentView().findViewById(R.id.deeper).setBackgroundResource(R.drawable.bg_rect_padded_normal_2);
					popup.getContentView().findViewById(R.id.warehouse).setBackgroundResource(R.drawable.bg_rect_padded_normal_2);
					v.setBackgroundResource(R.drawable.bg_rect_padded_pressed_2);
					switch(v.getId()) {
					case R.id.details:
						DetailsDialog dd = new DetailsDialog(getActivity());
						dd.start(o.PictInUse, o.Detailtext2);
//						Toast.makeText(getActivity(), "Details", Toast.LENGTH_SHORT).show();
						break;
					case R.id.higher:
						((MainActivity) getActivity()).showLayer1Fragment();
						//Toast.makeText(getActivity(), "higher", Toast.LENGTH_SHORT).show();
						break;
					case R.id.deeper:
//						Toast.makeText(getActivity(), "deeper", Toast.LENGTH_SHORT).show();
						break;
					case R.id.warehouse:
//						Toast.makeText(getActivity(), "warehouse", Toast.LENGTH_SHORT).show();
						sendData(adapter.formatLayer2());
//						sendFile(adapter.formatLayer2());
						break;
					}
				}
			};
			if(o.curled) {
				View details = popup.getContentView().findViewById(R.id.details);
				details.setVisibility(View.VISIBLE);
				details.setOnClickListener(popupListener);
				popup.getContentView().findViewById(R.id.sep1).setVisibility(View.VISIBLE);
			}
			popup.getContentView().findViewById(R.id.higher).setOnClickListener(popupListener);
			popup.getContentView().findViewById(R.id.deeper).setOnClickListener(popupListener);
			popup.getContentView().findViewById(R.id.warehouse).setOnClickListener(popupListener);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// when no data recieved from server, show a dialog
	public void showNoDataDialog() {
		final Dialog d = new Dialog(getActivity());
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_generic_ok);
		d.setCancelable(false);
		((TextView) d.findViewById(R.id.body)).setText("No entries found in this category. Please try other categories.");
		Button ok = (Button) d.findViewById(R.id.btnok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				d.dismiss();
				((MainActivity) getActivity()).showLayer1Fragment();
			}
		});
		d.show();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (hidden && adapter != null) {
			adapter.saveMarked();
			adapter.layer2.clear();
			adapter.notifyDataSetChanged();
		}
		if(hidden && popup != null) {
			popup.dismiss();
		}
	}

	@Override
	public void onDestroy() {

		super.onStop();
	}

	public void log(String msg) {
		Log.e(getClass().getName(), msg + "");
	}

	static class ViewHolder {
		public CustomTextView2 nameView = null;
	}

	public class Layer2Adapter extends ArrayAdapter<Layer2Object> implements SectionIndexer {
    	private String mSections = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		private Activity context = null;
		private List<Layer2Object> layer2 = null;

		public Layer2Adapter(Activity context, List<Layer2Object> objs) {
			super(context, R.layout.grid_item_layer2);
			this.context = context;
			this.layer2 = objs;
			Collections.sort(layer2, new Comparator<Layer2Object>() {

				@Override
				public int compare(Layer2Object lhs, Layer2Object rhs) {
					return lhs.sortField.compareTo(rhs.sortField);
					//return lhs.Name2.compareTo(rhs.Name2);
				}
			});
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View rowView = convertView;
			ViewHolder holder;
			// reuse row view, if possible
			if (rowView == null) {
				LayoutInflater inflater = context.getLayoutInflater();
				rowView = inflater.inflate(R.layout.grid_item_layer2, null);
				holder = new ViewHolder();
				holder.nameView = (CustomTextView2) rowView.findViewById(R.id.name);
				rowView.setTag(holder);
			}
			// set title and image url
			holder = (ViewHolder) rowView.getTag();
			final Layer2Object lo = layer2.get(position);
			holder.nameView.setBg(lo.isSelected, lo.curled);
			// name, brand, art no, stock, price
			String t = lo.Name2;
			t += lo.Brand2.length() <=0 ? "" : "\n" + lo.Brand2 ;
			t += lo.Artnr2.length() <=0 ? "" : "\n" + lo.Artnr2 ;
			t += lo.Stock2.length() <=0 ? "" : "\n" + lo.Stock2 ;
			t += (lo.Price2.length()  <=0 || lo.Price2.equals("0")) ? "" : "\n" + lo.Price2 ;
//			log(lo.Price2.length() <=0 ? "" : "\n" + lo.Price2);
			holder.nameView.setText(t);

			return rowView;
		}

		@Override
		public int getCount() {
			return this.layer2.size();
		}
		
		public int getSelectedCount() {
			int selected = 0;
			for (Layer2Object o : layer2) {
				if (o.isSelected) {
					selected++;
				}
			}
			return selected;
		}
		
		@Override
		public Layer2Object getItem(int position) {
			return this.layer2.get(position);
		}

		public List<Layer2Object> getData() {
			return this.layer2;
		}

		@Override
		public int getPositionForSection(int section) {
			// If there is no item for current section, previous section will be selected
			for (int i = section; i >= 0; i--) {
				for (int j = 0; j < getCount(); j++) {
					if (i == 0) {
						// For numeric section
						for (int k = 0; k <= 9; k++) {
							if (StringMatcher.match(String.valueOf(getItem(j).sortField.charAt(0)), String.valueOf(k)))
								return j;
						}
					} else {
						if (StringMatcher.match(String.valueOf(getItem(j).sortField.charAt(0)), String.valueOf(mSections.charAt(i))))
							return j;
					}
				}
			}
			return 0;
		}

		@Override
		public int getSectionForPosition(int position) {
			return 0;
		}

		@Override
		public Object[] getSections() {
			String[] sections = new String[mSections.length()];
			for (int i = 0; i < mSections.length(); i++)
				sections[i] = String.valueOf(mSections.charAt(i));
			return sections;
		}
		
		public String formatLayer2() {
			String format = Layer2Object.formatHead();
			for (Layer2Object o : layer2) {
				if(o.isSelected) {
					format += o.strFormat();
				}
			}
			return format;
		}
		
		public void saveMarked() {
			for (Layer2Object o: layer2) {
				if(o.isSelected) {
					Layer2ORM.update(getContext(), o);
				}
			}
		}
	}
	
}
