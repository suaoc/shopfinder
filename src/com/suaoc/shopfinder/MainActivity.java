package com.suaoc.shopfinder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {

	private final String prefs = "shopfinder_prefs";
	public ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
	boolean expired = false;
	boolean isRegistered = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// hide layer 2 fragment -- only show layer 1 fragment first
		Layer2Fragment l2f = (Layer2Fragment) getSupportFragmentManager().findFragmentById(R.id.layer2Fragment);
		getSupportFragmentManager().beginTransaction().hide(l2f).commit();

		properStart();
	}
	
	// check for main expiry, registration
	private void properStart() {
		isRegistered = verifyKey(getNameCompany()[3], getLicenseKey(), false);
		expired = expired();
		if(expired) {
			showExpiredDialog(false);
			return;
		} else if(!isRegistered) {
			if(Constants.IS_PLAY_STORE_VERSION) {
				showLicenseStoreDialog();
			} else {
				showLicenseWithKeyDialog(false, null);
			}
		} else {
			startCheckingMainEnd();
		}

		// register info button listener
		findViewById(R.id.appTitle).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showInfoButton();
			}
		});
	}
	
	// check if license has expired
	private boolean expired() {
		Date d = getFirstUse();
		if(d == null) {
			log("getFirstUse is null");
			return false;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		int duration = 0;
		if(isRegistered) {
			duration = getLicenseDuration();
			if(duration == -1) {
				return false;
			}
		} else if(Constants.IS_PLAY_STORE_VERSION){
			// store trial period is valid for 30 days
			duration = Constants.TRIAL_DAYS_STORE;
		} else {
			// personal trial period is 14 days
			duration = Constants.TRIAL_DAYS_PERSONAL;
		}
		c.add(Calendar.DATE, duration);
		Date now = new Date();
		log("Now: " + now.toString() + "; Expires on: " + c.getTime().toString());
		Date first = getFirstUse();
		boolean nowIsBeforeFirst = false;
		if(first != null) {
			nowIsBeforeFirst = first.after(now);
		}
		return (now.after(c.getTime()) || nowIsBeforeFirst);
	}
	
	// check main end on server
	private void checkMainEnd() {
		// show progress bar while checking MainEnd
		((Layer1Fragment) getSupportFragmentManager().findFragmentById(R.id.layer1Fragment)).showProgressBar();
		MainEndWorker mw = new MainEndWorker(this);
		mw.execute(Constants.URLs.mainEndURL);
	}
	
	// process response for main end from server
	public void processMainEnd(String response) {
		if(response == null) {
			log("MainEnd is null");
			Layer1Fragment l1f = (Layer1Fragment) getSupportFragmentManager().findFragmentById(R.id.layer1Fragment);
			// start layer 1 fragment, may be we have local data
			l1f.start(true, false);
//			Toast.makeText(this, "Failed to update", Toast.LENGTH_SHORT).show();
			return;
		}
		setLastUpdate();
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date serverMainEnd = null;
		try {
			serverMainEnd = format.parse(response);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String localMainEndStr = getMainEnd();
		log("local main end is: " + localMainEndStr);
		if(localMainEndStr.length() <= 0) {
			log("no local mainEnd. Update from server");
			updateLayer1FromServer(format.format(serverMainEnd), false);
			return;
		}
		Date localMainEnd = null;
		try {
			localMainEnd = format.parse(localMainEndStr);
			// localMainEnd = format.parse("08.01.2014");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		log(localMainEnd.toString());
		log(serverMainEnd.toString());
		if(localMainEnd.before(serverMainEnd)) {
			log("mainEnd on server has been modified to a later date. Update from server");
			showDatabaseUpdateDialog(localMainEndStr,format.format(serverMainEnd));
			return;
		}
		if(response.length() <= 0 ) {
			// if server mainEnd is empty, make localMainEnd empty as well, so every time load data from server
			setMainEnd("");
		}
		if(isFinishing()) {
			return;
		}
		// here we have a valid local date
		// load layer1 from local database
		Layer1Fragment l1f = (Layer1Fragment) getSupportFragmentManager().findFragmentById(R.id.layer1Fragment);
		l1f.start(true, false);
	}
	
	private void updateLayer1FromServer(String mainEndStr, boolean updating) {
		if(isFinishing()) {
			return;
		}
		// update now
		setMainEnd(mainEndStr);
		Layer1Fragment l1f = (Layer1Fragment) getSupportFragmentManager().findFragmentById(R.id.layer1Fragment);
		l1f.start(false, updating);
	}

	// show layer 1
	public void showLayer1Fragment() {
		FragmentManager fm = getSupportFragmentManager();
		Layer2Fragment l2f = (Layer2Fragment) fm.findFragmentById(R.id.layer2Fragment);
		Layer1Fragment l1f = (Layer1Fragment) fm.findFragmentById(R.id.layer1Fragment);
		fm.beginTransaction().hide(l2f).show(l1f).commit();
		((TextView) findViewById(R.id.layerName)).setText("Layer 1");
	}

	// show layer 2
	public void showLayer2Fragment(boolean local, ArrayList<String> filters) {
		FragmentManager fm = getSupportFragmentManager();
		Layer2Fragment l2f = (Layer2Fragment) fm.findFragmentById(R.id.layer2Fragment);
		fm.beginTransaction().hide(fm.findFragmentById(R.id.layer1Fragment)).show(l2f).commit();
		l2f.start(local, filters);
		((TextView) findViewById(R.id.layerName)).setText("Layer 2");
	}
	
	// ask what to do on app exit
	private void showOnExitDialog() {
		final Dialog ld = new Dialog(this);
		ld.requestWindowFeature(Window.FEATURE_NO_TITLE);
		ld.setContentView(R.layout.dialog_ask_on_exit);
		ld.setCancelable(false);
		((Button) ld.findViewById(R.id.btncancel)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ld.dismiss();
			}
		});
		((Button) ld.findViewById(R.id.btnno)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ld.dismiss();
				// unmark selected
				clearSelected();
				finish();
			}
		});
		// when yes clicked, remember all items on layer 2
		((Button) ld.findViewById(R.id.btnyes)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ld.dismiss();
				finish();
			}
		});
		ld.show();
	}

	// show that the license has expired
	private void showExpiredDialog(boolean fromInfoDialog) {
		if(Constants.IS_PLAY_STORE_VERSION) {
			// its play store version
			showStoreVersionExpired(fromInfoDialog);
			return;
		}
		// its personal version, so show expire dialog for personal version
		showLocalVersionExpired();
	}
	
	// show expire dialog for personal version
	private void showLocalVersionExpired() {
		final Dialog d = new Dialog(this);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_generic_ok);
		((TextView) d.findViewById(R.id.body)).setText("The testing period is over.\nPlease license the software now if you want to use it again.");
		((Button)d.findViewById(R.id.btnok)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showLicenseWithKeyDialog(false, d);
			}
		});
		((Button)d.findViewById(R.id.btnexit)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				d.dismiss();
				finish();
			}
			
		});
		d.show();
	}
	
	// show expire dialog for store version
	private void showStoreVersionExpired(final boolean fromInfoDialog) {
		final Dialog ld = new Dialog(this);
		ld.requestWindowFeature(Window.FEATURE_NO_TITLE);
		ld.setContentView(R.layout.dialog_expired);
		ld.setCancelable(false);
		((Button) ld.findViewById(R.id.btnUpdate)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO update to pro version
			}
		});
		((Button) ld.findViewById(R.id.btnnormalversion)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(!fromInfoDialog) {
					startCheckingMainEnd();
				}
				ld.dismiss();
			}
		});
		ld.show();
	}
	
	// show database update dialog
	private void showDatabaseUpdateDialog(String localDate, final String serverDate) {
		if(isFinishing()) {
			return;
		}
		final Dialog ld = new Dialog(this);
		ld.requestWindowFeature(Window.FEATURE_NO_TITLE);
		ld.setContentView(R.layout.dialog_database_update);
		// ((TextView) ld.findViewById(R.id.msg)).setText(String.format("The local database is valid till %s. The app will now try to update your local database.\nPlease verify that you are connected to internet.", localDate));
		((TextView) ld.findViewById(R.id.msg)).setText("The app will now try to update your local database.\nPlease verify that you are connected to internet.");
		ld.setCancelable(false);
		((Button) ld.findViewById(R.id.btnok)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(!isNetworkConnected()) {
					showEnableInternetDialog(false);
					return;
				}
				ld.dismiss();
				updateLayer1FromServer(serverDate, true);
			}
		});
		((Button) ld.findViewById(R.id.btncancel)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ld.dismiss();
				Layer1Fragment l1f = (Layer1Fragment) getSupportFragmentManager().findFragmentById(R.id.layer1Fragment);
				l1f.start(true, false);
			}
		});
		ld.show();
	}
	
	@Override
	public void onBackPressed() {
		Layer2Fragment l2f = (Layer2Fragment) getSupportFragmentManager().findFragmentById(R.id.layer2Fragment);
		if(!l2f.isHidden()){
			showLayer1Fragment();
			return;
		}
		if(!expired) {
			// if license is valid ask whether to remember layer 2 buttons
			showOnExitDialog();
		} else {
			// if license is not valid, don't remember layer 2 buttons
			clearSelected();
			super.onBackPressed();
		}
	}
	
	// clear selections for layer 2
	private void clearSelected() {
		Layer2ORM.clearSelected(getApplicationContext());
		((Layer1Fragment) getSupportFragmentManager().findFragmentById(R.id.layer1Fragment)).onExit();
	}
	
	// check main end after regular intervals
	public void startCheckingMainEnd() {
		scheduler.scheduleAtFixedRate(
			new Runnable() {
				public void run() {
					runOnUiThread(new Runnable() {
						public void run() {
							log("running scheduled task");
							checkMainEnd();
						}
					});
			    }
			}, 0, Constants.UPDATE_INTERVAL, TimeUnit.MINUTES);
   }

	// show license dialog
	public void showLicenseWithKeyDialog(final boolean fromInfoDialog, final Dialog testingOverDialog) {
		final Dialog ld = new Dialog(this);
		ld.requestWindowFeature(Window.FEATURE_NO_TITLE);
		ld.setContentView(R.layout.dialog_license_with_key);
		ld.setCancelable(false);
		
		final EditText fieldName = (EditText) ld.findViewById(R.id.fieldname);
		final EditText fieldCompany = (EditText) ld.findViewById(R.id.fieldCompany);
		final EditText fieldEmail = (EditText) ld.findViewById(R.id.fieldEmail);
		final EditText fieldCode = (EditText) ld.findViewById(R.id.fieldCode);
		final EditText fieldKey = (EditText) ld.findViewById(R.id.fieldKey);
		String d[] = getNameCompany();
		fieldName.setText(d[0]);
		fieldCompany.setText(d[1]);
		fieldEmail.setText(d[2]);
		fieldCode.setText(d[3]);
		if(d[3].length() > 0) {
			showLicenseKeyField(ld);
		}

		Button generate = (Button) ld.findViewById(R.id.btngenerate);
		generate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final String fieldNameStr = fieldName.getText().toString();
				final String fieldCompanyStr = fieldCompany.getText().toString();
				final String fieldEmailStr = fieldEmail.getText().toString();
				if(fieldNameStr == null || fieldNameStr.length() <= 0 || fieldCompanyStr == null || fieldCompanyStr.length() <= 0 || fieldEmailStr == null || fieldEmailStr.length() <= 0) {
					Toast.makeText(MainActivity.this, "Please enter name, company and email", Toast.LENGTH_SHORT).show();
					return;
				}
				// generate license code
				String code = UUID.nameUUIDFromBytes((fieldNameStr+fieldCompanyStr).getBytes()).toString().replace("-", "").substring(0, 11).toUpperCase(); 
				fieldCode.setText(code);
				putNameCompany(fieldNameStr, fieldCompanyStr, fieldEmailStr, code);
			}
		});
		
		Button cancel = (Button) ld.findViewById(R.id.btncancel);
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ld.dismiss();
				if(expired) {
//					Toast.makeText(getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();
					return;
				}
				if(!fromInfoDialog) {
					showInternetNeededDialog();
				}
				if(getFirstUse() == null && !expired) {
					setFirstUse();
				}
			}
		});
		
		Button send= (Button) ld.findViewById(R.id.btnsend);
		send.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final String fieldName = ((EditText) ld.findViewById(R.id.fieldname)).getText().toString();
				final String fieldCompany = ((EditText) ld.findViewById(R.id.fieldCompany)).getText().toString();
				final String fieldEmailStr = fieldEmail.getText().toString();
				String code = ((EditText) ld.findViewById(R.id.fieldCode)).getText().toString();
				if(code == null || code.length() <= 0) {
					Toast.makeText(MainActivity.this, "Please generate code first", Toast.LENGTH_SHORT).show();
					return;
				}
				if(!isNetworkConnected()) {
					showEnableInternetDialog(true);
					return;
				}
				Toast.makeText(getApplicationContext(), "Sending..", Toast.LENGTH_LONG).show();
				EmailWorker ew = new EmailWorker(MainActivity.this, fieldName + "," + fieldCompany + "," + code, fieldEmailStr);
				ew.execute(Constants.URLs.emailURL + "?key=0");
				// show key field
				showLicenseKeyField(ld);
			}
		});
		
		Button ok = (Button) ld.findViewById(R.id.btnok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String key = fieldKey.getText().toString();
				if(key == null || key.length() < 1) {
					Toast.makeText(getApplicationContext(), "Please enter a valid key or press Cancel.", Toast.LENGTH_SHORT).show();
					return;
				}
				if(verifyKey(fieldCode.getText().toString(), key, true)) {
					// valid key entered. start counting from now on
					showCorrectKeyDialog();
					if(testingOverDialog != null) {
						testingOverDialog.dismiss();
					}
				} else {
					Toast.makeText(getApplicationContext(), "Please enter a valid key or press Cancel.", Toast.LENGTH_SHORT).show();
					return;
				}
				ld.dismiss();
			}
		});
		ld.show();
	}
	
	// show license dialog for store version 
	public void showLicenseStoreDialog() {
		final Dialog ld = new Dialog(this);
		ld.requestWindowFeature(Window.FEATURE_NO_TITLE);
		ld.setContentView(R.layout.dialog_license_store);
		ld.setCancelable(false);
		((Button) ld.findViewById(R.id.btnok)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ld.dismiss();
				if(!isNetworkConnected()) {
					showEnableInternetDialog(false);
					return;
				}
				if(getFirstUse() == null && !expired) {
					setFirstUse();
				}
				startCheckingMainEnd();
			}
		});
		ld.show();
	}
	
	// remember license information
	private void storeLicenseInfo(String key, int duration) {
		setLicenseKey(key);
		setLicenseDuration(duration);
		// Toast.makeText(getApplicationContext(), "" + duration, Toast.LENGTH_SHORT).show();
		setFirstUse();
	}

	// check if a key is valid
	boolean verifyKey(String code, String key, boolean store) {
		try {
			int d = Constants.isMasterKey(key);
			if(d != 0) {
				if(store) {
					storeLicenseInfo(key, d);
				}
				return true;
			}
			String[] arr = LicenseToolActivity.decode(key).split(":");
			// arr[0] is valid-till date, arr[1] is license code, arr[2] is number of days valid
			SimpleDateFormat format = new SimpleDateFormat("ddMMyyHH");
			Date validTill = format.parse(arr[0]);
			Date nowD = new Date();
			log(nowD.toString() + " : " + validTill.toString());
			if(nowD.after(validTill)) {
				// invalid key
				log("invalid key entered.");
				return false;
			}
			if(code.equalsIgnoreCase(arr[1])) {
				if(store) {
					storeLicenseInfo(key, Integer.parseInt(arr[2]));
				}
				return true;
			}
		}catch(Exception e) {
			log("Verification failed! Please make sure you entered the right key.");
		}
		return false;
	}

	// show license key field
	private void showLicenseKeyField(final Dialog ld) {
		EditText e = (EditText) ld.findViewById(R.id.fieldKey);
		e.setVisibility(View.VISIBLE);
		ld.findViewById(R.id.keyLabel).setVisibility(View.VISIBLE);
		
		Button ok = (Button) ld.findViewById(R.id.btnok);
		ok.setVisibility(View.VISIBLE);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ld.dismiss();
				showInternetNeededDialog();
			}
		});
	}
	
	// show warning that internet connection is needed
	public void showInternetNeededDialog() {
		final Dialog d = new Dialog(this);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_internet_required);
		d.setCancelable(false);
		Button ok = (Button) d.findViewById(R.id.btnok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// check internet
				if (!isNetworkConnected() && getLayer1() == null) {
					// show enable internet dialog
					showEnableInternetDialog(false);
				} else {
					// check for updates on server
					startCheckingMainEnd();
				}
				d.dismiss();
			}
		});
		d.show();
	}
	
	// show dialog to enable internet
	public void showEnableInternetDialog(final boolean dontCheckMain) {
		final Dialog d = new Dialog(this);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_enable_internet);
		d.setCancelable(false);
		Button ok = (Button) d.findViewById(R.id.btnok);
		ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// show network settings screen
				if(dontCheckMain) {
					startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS), SETTINGS_SCREEN_CODE_GO_ON);
				} else {
					startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS), SETTINGS_SCREEN_CODE);
				}
				d.dismiss();
			}
		});
		d.show();
	}
	
	// no internet found, show exit dialog
	public void showNoInternetDialog() {
		final Dialog d = new Dialog(this);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_no_internet);
		d.setCancelable(false);
		Button ok = (Button) d.findViewById(R.id.btnExit);
		ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// no internet, so close activity
				d.dismiss();
				finish();
			}
		});
		d.show();
	}

	// show dialog which says correct key was inserted
	public void showCorrectKeyDialog() {
		final Dialog d = new Dialog(this);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_correct_key);
		d.setCancelable(false);
		Button ok = (Button) d.findViewById(R.id.btnok);
		ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// check for updates on server
				startCheckingMainEnd();
				d.dismiss();
			}
		});
		d.show();
	}
	
	// show info dialog with license, build nr, and license type
	private void showInfoDialog() {
		String[] a = getNameCompany();
		Calendar c = Calendar.getInstance();
		String suffix =  c.get(Calendar.YEAR) + "";
		String version = "";
		try {
			suffix = suffix.substring(2, 4);
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			version = Utils.encode(pInfo.versionName);
		} catch (Exception e) {
		}
		String licensee = "Unlicensed";
		String l = getLicenseKey();
		if(l.length() > 4) {
			licensee = a[0] + ", " + a[1];
		}
		
		int du = getLicenseDuration() / 7;
		suffix += du < 10 ? "0" + du : "" + du;
		
		String build = version +  "-" + suffix;
		String lastUpdated = getLastUpdated();
		final Dialog d = new Dialog(this);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_info);
		((TextView) d.findViewById(R.id.build)).setText("Build-Nr: " + build.replace("\n", "").replace("=", ""));
		
		TextView t = ((TextView) d.findViewById(R.id.licenseType));
		t.setText(isRegistered ? "Type: Licensed" : "Type: Normal" );
		t.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showExpiredDialog(true);
			}
		});
		if(!Constants.IS_PLAY_STORE_VERSION) {
			t.setVisibility(View.GONE);
		}
		TextView lv = ((TextView) d.findViewById(R.id.licenseFor));
		lv.setText("License for " + licensee);
		lv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showLicenseWithKeyDialog(true, null);
			}
		});
		((TextView) d.findViewById(R.id.lastUpdated)).setText("Last database connect: " + lastUpdated);

		d.show();
	}
	
	// show info button
	private void showInfoButton() {
		final Dialog d2 = new Dialog(this);
		d2.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		Window window = d2.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();

		wlp.gravity = Gravity.BOTTOM | Gravity.LEFT;
		wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(wlp);
		
		ImageView i = new ImageView(this);
		i.setBackgroundResource(R.drawable.info_btn);
		i.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				d2.dismiss();
				showInfoDialog();
			}
		});
		d2.setContentView(i);
		d2.show();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		showInfoButton();
		return true;
	}

	@Override
	protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
		if(requestCode == SETTINGS_SCREEN_CODE || requestCode == SETTINGS_SCREEN_CODE_GO_ON) {
			// we are back from settings screen
			final Layer1Fragment l1f = ((Layer1Fragment) getSupportFragmentManager().findFragmentById(R.id.layer1Fragment));
			l1f.showProgressBar();
			Handler h = new Handler();
			Runnable r = new Runnable() {
				
				@Override
				public void run() {
					try {
						if(!isNetworkConnected()) {
							// we dont have internet
							l1f.hideProgressBar();
							// show exit dialog
							showNoInternetDialog();
						} else {
							if(requestCode == SETTINGS_SCREEN_CODE_GO_ON) {
								l1f.hideProgressBar();
							} else {
								startCheckingMainEnd();
							}
						}
					}catch(Exception e) {}					
				}
			};
			h.postDelayed(r, 4000);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private final int SETTINGS_SCREEN_CODE = 0;
	private final int SETTINGS_SCREEN_CODE_GO_ON = 1;
	
	@Override
	public void finish() {
		// save preferences
		if(scheduler!=null) {
			scheduler.shutdownNow();
		}
		super.finish();
	}

	// save personal information
	public void putNameCompany(String name, String company, String email, String code) {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		sp.edit().putString(Constants.Keys.NAME, name).putString(Constants.Keys.COMPANY, company).putString(Constants.Keys.EMAIL, email).putString(Constants.Keys.LICENSE_CODE, code).commit();
	}

	// get personal information
	public String[] getNameCompany() {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		String a[] = { sp.getString(Constants.Keys.NAME, ""), sp.getString(Constants.Keys.COMPANY, ""), sp.getString(Constants.Keys.EMAIL, ""), sp.getString(Constants.Keys.LICENSE_CODE, "")};
		return a;
	}

	// get local main end
	public String getMainEnd() {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		return sp.getString(Constants.Keys.MAIN_END, "");
	}

	// save main end locally
	public void setMainEnd(String v) {
		log("putMainEnd " + v);
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		sp.edit().putString(Constants.Keys.MAIN_END, v).commit();
	}

	// get stored license 
	public String getLicenseKey() {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		return sp.getString(Constants.Keys.LICENSE_KEY, "");
	}

	// store license key
	public void setLicenseKey(String v) {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		sp.edit().putString(Constants.Keys.LICENSE_KEY, v).commit();
	}

	// get license duaration
	public int getLicenseDuration() {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		return sp.getInt(Constants.Keys.LICENSE_DURATION, 0);
	}

	// get license duration
	public void setLicenseDuration(int v) {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		sp.edit().putInt(Constants.Keys.LICENSE_DURATION, v).commit();
	}

	// set database last access
	public void setLastUpdate() {
		SimpleDateFormat format = new SimpleDateFormat("ddMMyy-HH:mm");
		Date now = new Date();
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		sp.edit().putString(Constants.Keys.LAST_UPDATED, format.format(now)).commit();
	}

	// get time for database last access
	public String getLastUpdated() {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		return sp.getString(Constants.Keys.LAST_UPDATED, "");
	}

	// set whether to show/hide info dialog for layer 2
	public void setDontShow(boolean value) {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		sp.edit().putBoolean("DONT_SHOW", value).commit();
	}

	// check if we have to show info dialog for layer 2
	public boolean getDontShow() {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		return sp.getBoolean("DONT_SHOW", false);
	}

	// set the date the app was opened for first time
	public void setFirstUse() {
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date now = new Date();
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		sp.edit().putString(Constants.Keys.FIRST_USE, format.format(now)).commit();
	}

	// get date of first use
	public Date getFirstUse() {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		String date = sp.getString(Constants.Keys.FIRST_USE, "");
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		try {
			return format.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	// save data for layer 1
	public boolean saveLayer1(String json) {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		return sp.edit().putString(Constants.Keys.LAYER1_JSON, json).commit();
	}

	// get data for layer 1
	public String getLayer1() {
		SharedPreferences sp = getSharedPreferences(prefs, MODE_PRIVATE);
		return sp.getString(Constants.Keys.LAYER1_JSON, null);
	}
	
	// check network connectivity
	public boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() == null) {
			// There are no active networks.
			return false;
		}
		return true;
	}
	
	public void log(String msg){
		if(!Constants.LOG) {
			return;
		}
		Log.e(getClass().getName(), msg);
	}
}
