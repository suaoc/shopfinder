package com.suaoc.shopfinder;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

public class CustomTextView2 extends TextView{

	boolean isSelected = false;
	
	public CustomTextView2(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	public CustomTextView2(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public CustomTextView2(Context context) {
		super(context);
		init();
	}
	
	private void init() {
		//setOnClickListener(clickListener);
	}

	public void toggle(boolean isSelected, boolean isCurled) {
		if(!isSelected) {
			isSelected = true;
		} else {
			isSelected = false;
		}
		setBg(isSelected, isCurled);
	}
	
	public void setBg(boolean select, boolean isCurled) {
		if(isCurled) {
			if(select) {
				((View)getParent()).setBackgroundResource(R.drawable.curl_pressed);
			} else {
				((View)getParent()).setBackgroundResource(R.drawable.curl_normal);
			}
			return;
		}
		if(select) {
			// ((View)getParent()).setBackgroundColor(0xff4a789c);
			((View)getParent()).setBackgroundResource(R.drawable.btn2_pressed);
		} else {
			// ((View)getParent()).setBackgroundColor(0xff213a4e);
			((View)getParent()).setBackgroundResource(R.drawable.btn2_normal);
		}
	}

//    button active: #4a789c
//    button inactive: #213a4e
}
