package com.suaoc.shopfinder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Layer1Worker extends AsyncTask<String, Integer, Boolean>{

	protected Layer1Fragment ml = null;
	List<Layer1Object> layer1 = new ArrayList<Layer1Object>();
	
	public Layer1Worker(Layer1Fragment ml) {
		this.ml = ml;
	}
	
	boolean isLocal = false;
	String localData = "";
	
	public AsyncTask<String, Integer, Boolean> start(boolean local, String data, String url) {
		isLocal = local;
		localData = data;
		return execute(url);
	}
	
	// create objects from string
	private void parse(String response) {
        JsonParser parser = new JsonParser();
        JsonArray objects = null;
        if(isLocal) {
        	objects = (JsonArray) parser.parse(response);
        } else {
	    	JsonObject element = (JsonObject)parser.parse(response);
	    	objects = (JsonArray) element.get("objects");
        }
        final GsonBuilder gsonBuilder = new GsonBuilder();
        final Gson gson = gsonBuilder.create();
        
    	for(int i=0; i < objects.size(); i++) {
			Layer1Object o = gson.fromJson(objects.get(i), Layer1Object.class);
			layer1.add(o);
			
		}
	}

	@Override
	protected Boolean doInBackground(String... params) {
		try {
			String response = "";
			if(isLocal) {
				response = localData;
			} else {
				response = getDataFromServer(params[0]);
			}
			parse(response);
		}catch(UnknownHostException uhe){
			log("Unknown host exception");
			uhe.printStackTrace();
			return false;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
        return true;
    }

	// get layer 1 data from server
	private String getDataFromServer(String url) throws ClientProtocolException, IOException {
		InputStream inputStream = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		
		HttpResponse httpResponse = httpClient.execute(httpGet);
		HttpEntity httpEntity = httpResponse.getEntity();
		inputStream = httpEntity.getContent();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;
		String response = "";
		while((line = br.readLine()) != null) {
			// log(line);
			response += line;
		}
		br.close();
		inputStream.close();
		return response;

	}
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if(!result) {
			// failed to get the json data
			//Toast.makeText(ml.getActivity(), "An error occurred. Please try later with internet connection!", Toast.LENGTH_SHORT).show();
			ml.updateGridView(layer1, true);
			return;
		}
		if(ml.getActivity().isFinishing()) {
			return;
		}
		// set last database access
		((MainActivity) ml.getActivity()).setLastUpdate();
		// update layer 1 list 
		ml.updateGridView(layer1, false);
	}
	
	public void log(String msg){
		Log.e(getClass().getName(), msg);
	}
	
}