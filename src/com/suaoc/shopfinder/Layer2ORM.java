package com.suaoc.shopfinder;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Layer2ORM {

    public static final String TAG = "Layer2ORM";
    public static final String TABLE_NAME = "layer2";
    private static final String COMMA_SEP = ", ";

    private static final String COLUMN_ID_TYPE = "INTEGER PRIMARY KEY";
    private static final String COLUMN_ID = "LocalId";

    private static final String COLUMN_SERVER_ID_TYPE = "INTEGER";
    private static final String COLUMN_SERVER_ID = "ServerId";

    public static final String COLUMN_ADDITION_TYPE = "TEXT";
    public static final String COLUMN_ADDITION = "Addition";

    public static final String COLUMN_ARTNR2_TYPE = "TEXT";
    public static final String COLUMN_ARTNR2 = "Artnr2";

    public static final String COLUMN_BP2_TYPE = "TEXT";
    public static final String COLUMN_BP2 = "Bp2";

    public static final String COLUMN_BRAND2_TYPE = "TEXT";
    public static final String COLUMN_BRAND2 = "Brand2";

    public static final String COLUMN_CAT2_TYPE = "TEXT";
    public static final String COLUMN_CAT2 = "Cat2";

    public static final String COLUMN_CONCURRENT_TYPE = "TEXT";
    public static final String COLUMN_CONCURRENT = "Concurrent";

    public static final String COLUMN_DATAEND2_TYPE = "TEXT";
    public static final String COLUMN_DATAEND2 = "Dataend2";

    public static final String COLUMN_DETAILPICT2L_TYPE = "TEXT";
    public static final String COLUMN_DETAILPICT2L = "Detailpict2l";

    public static final String COLUMN_DETAILPICT2M_TYPE = "TEXT";
    public static final String COLUMN_DETAILPICT2M = "Detailpict2m";

    public static final String COLUMN_DETAILPICT2S_TYPE = "TEXT";
    public static final String COLUMN_DETAILPICT2S = "Detailpict2s";

    public static final String COLUMN_DETAILPICT2XL_TYPE = "TEXT";
    public static final String COLUMN_DETAILPICT2XL = "Detailpict2xl";

    public static final String COLUMN_DETAILPICT2XS_TYPE = "TEXT";
    public static final String COLUMN_DETAILPICT2XS = "Detailpict2xs";

    public static final String COLUMN_DETAILPICT2XXL_TYPE = "TEXT";
    public static final String COLUMN_DETAILPICT2XXL = "Detailpict2xxl";

    public static final String COLUMN_DETAILPICT2XXS_TYPE = "TEXT";
    public static final String COLUMN_DETAILPICT2XXS = "Detailpict2xxs";

    public static final String COLUMN_DETAILTEXT2_TYPE = "TEXT";
    public static final String COLUMN_DETAILTEXT2 = "Detailtext2";

    public static final String COLUMN_FILTER1_TYPE = "TEXT";
    public static final String COLUMN_FILTER1 = "Filter1";

    public static final String COLUMN_INCLUDED_TYPE = "TEXT";
    public static final String COLUMN_INCLUDED = "Included";

    public static final String COLUMN_LEVELH_TYPE = "TEXT";
    public static final String COLUMN_LEVELH = "Levelh";

    public static final String COLUMN_NAME2_TYPE = "TEXT";
    public static final String COLUMN_NAME2 = "Name2";

    public static final String COLUMN_PDFURL2_TYPE = "TEXT";
    public static final String COLUMN_PDFURL2 = "Pdfurl2";

    public static final String COLUMN_PRICE2_TYPE = "TEXT";
    public static final String COLUMN_PRICE2 = "Price2";

    public static final String COLUMN_PRICEEND3_TYPE = "TEXT";
    public static final String COLUMN_PRICEEND3 = "Priceend3";

    public static final String COLUMN_PRICENEW2_TYPE = "TEXT";
    public static final String COLUMN_PRICENEW2 = "Pricenew2";

    public static final String COLUMN_PU2_TYPE = "TEXT";
    public static final String COLUMN_PU2 = "Pu2";

    public static final String COLUMN_SAVING2_TYPE = "TEXT";
    public static final String COLUMN_SAVING2 = "Saving2";

    public static final String COLUMN_SPECIAL2_TYPE = "TEXT";
    public static final String COLUMN_SPECIAL2 = "Special2";

    public static final String COLUMN_STOCK2_TYPE = "TEXT";
    public static final String COLUMN_STOCK2 = "Stock2";

    public static final String COLUMN_STOCKEND2_TYPE = "TEXT";
    public static final String COLUMN_STOCKEND2 = "Stockend2";

    public static final String COLUMN_SELECTED_TYPE = "INTEGER";
    public static final String COLUMN_SELECTED = "isSelected";

    public static final String COLUMN_CURLED_TYPE = "INTEGER";
    public static final String COLUMN_CURLED = "curled";

    public static final String COLUMN_IMAGE_TYPE = "TEXT";
    public static final String COLUMN_IMAGE = "Image";

    public static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
            COLUMN_ID + " " + COLUMN_ID_TYPE + COMMA_SEP +
            COLUMN_SERVER_ID + " " + COLUMN_SERVER_ID_TYPE + COMMA_SEP +
			COLUMN_ADDITION + " " + COLUMN_ADDITION_TYPE + COMMA_SEP +
			COLUMN_ARTNR2 + " " + COLUMN_ARTNR2_TYPE + COMMA_SEP +
			COLUMN_BP2 + " " + COLUMN_BP2_TYPE + COMMA_SEP +
			COLUMN_BRAND2 + " " + COLUMN_BRAND2_TYPE + COMMA_SEP +
			COLUMN_CAT2 + " " + COLUMN_CAT2_TYPE + COMMA_SEP +
			COLUMN_CONCURRENT + " " + COLUMN_CONCURRENT_TYPE + COMMA_SEP +
			COLUMN_DATAEND2 + " " + COLUMN_DATAEND2_TYPE + COMMA_SEP +
			COLUMN_DETAILPICT2L + " " + COLUMN_DETAILPICT2L_TYPE + COMMA_SEP +
			COLUMN_DETAILPICT2M + " " + COLUMN_DETAILPICT2M_TYPE + COMMA_SEP +
			COLUMN_DETAILPICT2S + " " + COLUMN_DETAILPICT2S_TYPE + COMMA_SEP +
			COLUMN_DETAILPICT2XL + " " + COLUMN_DETAILPICT2XL_TYPE + COMMA_SEP +
			COLUMN_DETAILPICT2XS + " " + COLUMN_DETAILPICT2XS_TYPE + COMMA_SEP +
			COLUMN_DETAILPICT2XXL + " " + COLUMN_DETAILPICT2XXL_TYPE + COMMA_SEP +
			COLUMN_DETAILPICT2XXS + " " + COLUMN_DETAILPICT2XXS_TYPE + COMMA_SEP +
			COLUMN_DETAILTEXT2 + " " + COLUMN_DETAILTEXT2_TYPE + COMMA_SEP +
			COLUMN_FILTER1 + " " + COLUMN_FILTER1_TYPE + COMMA_SEP +
			COLUMN_INCLUDED + " " + COLUMN_INCLUDED_TYPE + COMMA_SEP +
			COLUMN_LEVELH + " " + COLUMN_LEVELH_TYPE + COMMA_SEP +
			COLUMN_NAME2 + " " + COLUMN_NAME2_TYPE + COMMA_SEP +
			COLUMN_PDFURL2 + " " + COLUMN_PDFURL2_TYPE + COMMA_SEP +
			COLUMN_PRICE2 + " " + COLUMN_PRICE2_TYPE + COMMA_SEP +
			COLUMN_PRICEEND3 + " " + COLUMN_PRICEEND3_TYPE + COMMA_SEP +
			COLUMN_PRICENEW2 + " " + COLUMN_PRICENEW2_TYPE + COMMA_SEP +
			COLUMN_PU2 + " " + COLUMN_PU2_TYPE + COMMA_SEP +
			COLUMN_SAVING2 + " " + COLUMN_SAVING2_TYPE + COMMA_SEP +
			COLUMN_SPECIAL2 + " " + COLUMN_SPECIAL2_TYPE + COMMA_SEP +
			COLUMN_STOCK2 + " " + COLUMN_STOCK2_TYPE + COMMA_SEP +
			COLUMN_STOCKEND2 + " " + COLUMN_STOCKEND2_TYPE + COMMA_SEP +
			COLUMN_SELECTED + " " + COLUMN_SELECTED_TYPE + COMMA_SEP +
			COLUMN_IMAGE + " " + COLUMN_IMAGE_TYPE + COMMA_SEP +
			COLUMN_CURLED + " " + COLUMN_CURLED_TYPE +
			")";

    public static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    /**
     * Fetches the full list of objects stored in the local Database
     * @param context
     * @return
     */
    public static List<Layer2Object> getAll(Context context, ArrayList<String> filters) {
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase database = databaseWrapper.getReadableDatabase();

        String where = " where ";
        for (int i = 0; i < filters.size(); i++) {
			where += "Levelh='" + filters.get(i) + "'";
			if(i < filters.size() - 1) {
				where += " OR ";
			}
		}
        List<Layer2Object> list = null;
        if(database != null) {
            Cursor cursor = database.rawQuery("SELECT * FROM " + Layer2ORM.TABLE_NAME + where, null);
            Log.i(TAG, "Loaded " + cursor.getCount() + " objects...");
            if(cursor.getCount() > 0) {
                list = new ArrayList<Layer2Object>();
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                	Layer2Object l2o = cursorToObject(cursor);
                	if(l2o.ServerId != -1) {
    		    		l2o.sortField = l2o.Name2;
    		    		if(l2o.Name2.length() < 1) {
    		    			l2o.sortField = l2o.Brand2;
    		    		}
                		list.add(l2o);
                	}
                    cursor.moveToNext();
                }
                Log.i(TAG, "objects loaded successfully.");
            }
            database.close();
        }
        return list;
    }

    /**
     * Fetches a single Object identified by the specified ID
     * @param context
     * @param objectId
     * @return
     */
    public static Layer2Object findById(Context context, long objectId) {
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase database = databaseWrapper.getReadableDatabase();

        Layer2Object l2o = null;
        if(database != null) {
            Log.i(TAG, "Loading object["+objectId+"]...");
            Cursor cursor = database.rawQuery("SELECT * FROM " + Layer2ORM.TABLE_NAME + " WHERE " + Layer2ORM.COLUMN_SERVER_ID + " = " + objectId, null);

            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                l2o = cursorToObject(cursor);
                Log.i(TAG, "Object loaded successfully!");
            }

            database.close();
        }

        return l2o;
    }
    
    public static boolean insert(Context context, Layer2Object l2o) {
        if(l2o.ServerId >= 0 && findById(context, l2o.ServerId) != null) {
            Log.i(TAG, "Object already exists in database, not inserting!");
            return true;//update(context, l2o);
        }
        ContentValues values = objToContentValues(l2o);
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase database = databaseWrapper.getWritableDatabase();
        boolean success = false;
        try {
            if (database != null) {
                long objectId = database.insert(Layer2ORM.TABLE_NAME, "null", values);
                Log.i(TAG, "Inserted new Object with ID: " + objectId);
                success = true;
            }
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to insert object[" + l2o.ServerId + "] due to: " + ex);
        } finally {
            if(database != null) {
                database.close();
            }
        }
        return success;
    }

    public static boolean update(Context context, Layer2Object l2o) {
        ContentValues values = objToContentValues(l2o);
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase database = databaseWrapper.getWritableDatabase();

        boolean success = false;
        try {
            if (database != null) {
                Log.i(TAG, "Updating object[" + l2o.ServerId + "]...");
                database.update(Layer2ORM.TABLE_NAME, values, Layer2ORM.COLUMN_SERVER_ID + " = " + l2o.ServerId, null);
                success = true;
            }
        } catch (NullPointerException ex) {
            Log.e(TAG, "Failed to update object[" + l2o.ServerId + "] due to: " + ex);
        } finally {
            if(database != null) {
                database.close();
            }
        }
        return success;
    }
    
    //update layer2 set isSelected =0 where isSelected=1;
    
    public static ArrayList<String> clearSelected(Context context) {
    	// select distinct  Levelh from layer2;
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase database = databaseWrapper.getReadableDatabase();
        ArrayList<String> categories = null;
        if(database != null) {
            Cursor cursor = database.rawQuery("UPDATE " + Layer2ORM.TABLE_NAME + " SET isSelected=0 WHERE isSelected=1", null);
            categories = new ArrayList<String>(cursor.getCount());
            
            Log.i(TAG, "Loaded " + cursor.getCount() + " objects...");
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                	String Levelh = cursor.getString(cursor.getColumnIndex(COLUMN_LEVELH));
                    categories.add(Levelh);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Objects loaded successfully.");
            }
            database.close();
        }
        return categories;
    }

    private static ContentValues objToContentValues(Layer2Object l2o) {
        ContentValues values = new ContentValues();
        values.put(Layer2ORM.COLUMN_SERVER_ID, l2o.ServerId);
        values.put(Layer2ORM.COLUMN_ADDITION, l2o.Addition);
        values.put(Layer2ORM.COLUMN_ARTNR2, l2o.Artnr2);
        values.put(Layer2ORM.COLUMN_BP2, l2o.Bp2);
        values.put(Layer2ORM.COLUMN_BRAND2, l2o.Brand2);
        values.put(Layer2ORM.COLUMN_CAT2, l2o.Cat2);
        values.put(Layer2ORM.COLUMN_CONCURRENT, l2o.Concurrent);
        values.put(Layer2ORM.COLUMN_DATAEND2, l2o.Dataend2);
        values.put(Layer2ORM.COLUMN_DETAILPICT2L, l2o.Detailpict2l);
        values.put(Layer2ORM.COLUMN_DETAILPICT2M, l2o.Detailpict2m);
        values.put(Layer2ORM.COLUMN_DETAILPICT2S, l2o.Detailpict2s);
        values.put(Layer2ORM.COLUMN_DETAILPICT2XL, l2o.Detailpict2xl);
        values.put(Layer2ORM.COLUMN_DETAILPICT2XS, l2o.Detailpict2xs);
        values.put(Layer2ORM.COLUMN_DETAILPICT2XXL, l2o.Detailpict2xxl);
        values.put(Layer2ORM.COLUMN_DETAILPICT2XXS, l2o.Detailpict2xxs);
        values.put(Layer2ORM.COLUMN_DETAILTEXT2, l2o.Detailtext2);
        values.put(Layer2ORM.COLUMN_FILTER1, l2o.Filter1);
        values.put(Layer2ORM.COLUMN_INCLUDED, l2o.Included);
        values.put(Layer2ORM.COLUMN_LEVELH, l2o.Levelh);
        values.put(Layer2ORM.COLUMN_NAME2, l2o.Name2);
        values.put(Layer2ORM.COLUMN_PDFURL2, l2o.Pdfurl2);
        values.put(Layer2ORM.COLUMN_PRICE2, l2o.Price2);
        values.put(Layer2ORM.COLUMN_PRICEEND3, l2o.Priceend3);
        values.put(Layer2ORM.COLUMN_PRICENEW2, l2o.Pricenew2);
        values.put(Layer2ORM.COLUMN_PU2, l2o.Pu2);
        values.put(Layer2ORM.COLUMN_SAVING2, l2o.Saving2);
        values.put(Layer2ORM.COLUMN_SPECIAL2, l2o.Special2);
        values.put(Layer2ORM.COLUMN_STOCK2, l2o.Stock2);
        values.put(COLUMN_STOCKEND2, l2o.Stockend2);
        values.put(COLUMN_SELECTED, l2o.isSelected ? 1 : 0);
        values.put(COLUMN_CURLED, l2o.curled ? 1 : 0);
        values.put(Layer2ORM.COLUMN_IMAGE, l2o.PictInUse);
        return values;
    }

    private static Layer2Object cursorToObject(Cursor cursor) {
    	Layer2Object l2o = new Layer2Object();
//        l2o.localId = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
        l2o.ServerId = cursor.getLong(cursor.getColumnIndex(COLUMN_SERVER_ID));
        l2o.Addition = cursor.getString(cursor.getColumnIndex(COLUMN_ADDITION));
        l2o.Artnr2 = cursor.getString(cursor.getColumnIndex(COLUMN_ARTNR2));
        l2o.Bp2 = cursor.getString(cursor.getColumnIndex(COLUMN_BP2));
        l2o.Brand2 = cursor.getString(cursor.getColumnIndex(COLUMN_BRAND2));
        l2o.Cat2 = cursor.getString(cursor.getColumnIndex(COLUMN_CAT2));
        l2o.Concurrent = cursor.getString(cursor.getColumnIndex(COLUMN_CONCURRENT));
        l2o.Dataend2 = cursor.getString(cursor.getColumnIndex(COLUMN_DATAEND2));
        l2o.Detailpict2l = cursor.getString(cursor.getColumnIndex(COLUMN_DETAILPICT2L));
        l2o.Detailpict2m = cursor.getString(cursor.getColumnIndex(COLUMN_DETAILPICT2M));
        l2o.Detailpict2s = cursor.getString(cursor.getColumnIndex(COLUMN_DETAILPICT2S));
        l2o.Detailpict2xl = cursor.getString(cursor.getColumnIndex(COLUMN_DETAILPICT2XL));
        l2o.Detailpict2xs = cursor.getString(cursor.getColumnIndex(COLUMN_DETAILPICT2XS));
        l2o.Detailpict2xxl = cursor.getString(cursor.getColumnIndex(COLUMN_DETAILPICT2XXL));
        l2o.Detailpict2xxs = cursor.getString(cursor.getColumnIndex(COLUMN_DETAILPICT2XXS));
        l2o.Detailtext2 = cursor.getString(cursor.getColumnIndex(COLUMN_DETAILTEXT2));
        l2o.Filter1 = cursor.getString(cursor.getColumnIndex(COLUMN_FILTER1));
        l2o.Included = cursor.getString(cursor.getColumnIndex(COLUMN_INCLUDED));
        l2o.Levelh = cursor.getString(cursor.getColumnIndex(COLUMN_LEVELH));
        l2o.Name2 = cursor.getString(cursor.getColumnIndex(COLUMN_NAME2));
        l2o.Pdfurl2 = cursor.getString(cursor.getColumnIndex(COLUMN_PDFURL2));
        l2o.Price2 = cursor.getString(cursor.getColumnIndex(COLUMN_PRICE2));
        l2o.Priceend3 = cursor.getString(cursor.getColumnIndex(COLUMN_PRICEEND3));
        l2o.Pricenew2 = cursor.getString(cursor.getColumnIndex(COLUMN_PRICENEW2));
        l2o.Pu2 = cursor.getString(cursor.getColumnIndex(COLUMN_PU2));
        l2o.Saving2 = cursor.getString(cursor.getColumnIndex(COLUMN_SAVING2));
        l2o.Special2 = cursor.getString(cursor.getColumnIndex(COLUMN_SPECIAL2));
        l2o.Stock2 = cursor.getString(cursor.getColumnIndex(COLUMN_STOCK2));
        l2o.Stockend2 = cursor.getString(cursor.getColumnIndex(COLUMN_STOCKEND2));
        l2o.isSelected = cursor.getInt(cursor.getColumnIndex(COLUMN_SELECTED)) == 1 ? true : false;
        l2o.curled = cursor.getInt(cursor.getColumnIndex(COLUMN_CURLED)) == 1 ? true : false;
        l2o.PictInUse = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE));
        return l2o;
    }
    
    public static ArrayList<String> getLocalSavedLevelh(Context context) {
    	// select distinct  Levelh from layer2;
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase database = databaseWrapper.getReadableDatabase();
        ArrayList<String> categories = null;
        if(database != null) {
            Cursor cursor = database.rawQuery("SELECT DISTINCT Levelh FROM " + Layer2ORM.TABLE_NAME, null);
            categories = new ArrayList<String>(cursor.getCount());
            
            Log.i(TAG, "Loaded " + cursor.getCount() + " objects...");
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                	String Levelh = cursor.getString(cursor.getColumnIndex(COLUMN_LEVELH));
                    categories.add(Levelh);
                    cursor.moveToNext();
                }
                Log.i(TAG, "Objects loaded successfully.");
            }
            database.close();
        }
        return categories;
    }
    
    public static boolean clearAll(Context context) {
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase database = databaseWrapper.getWritableDatabase();
        boolean success = false;
        try {
            if (database != null) {
            	database.execSQL(SQL_DROP_TABLE);
            	database.execSQL(Layer2ORM.SQL_CREATE_TABLE);
            }
        } catch (NullPointerException ex) {
        } finally {
            if(database != null) {
                database.close();
            }
        }
        return success;
    }
    
}