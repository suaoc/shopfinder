package com.suaoc.shopfinder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class EmailWorker extends AsyncTask<String, Integer, Boolean>{

	String msgBody = null;
	Activity activity = null;
	String email = null;
	
	public EmailWorker(Activity ma, String s, String e) {
		this.activity = ma;
		this.msgBody = s;
		this.email = e;
	}

	@Override
	protected Boolean doInBackground(String... params) {
		String response = "";
		InputStream inputStream = null;
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			String u = params[0] + "&msg=" + URLEncoder.encode(msgBody) + "&email=" + URLEncoder.encode(email);
			log("Email url: " + u);
			HttpGet post = new HttpGet(u);

/*			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("email", email));
	        nameValuePairs.add(new BasicNameValuePair("msg", msgBody));
	        post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
*/
			HttpResponse httpResponse = httpClient.execute(post);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputStream = httpEntity.getContent();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			while((line = br.readLine()) != null) {
				// log(line);
				response += line;
			}
			log(response);
	        JsonParser parser = new JsonParser();
        	JsonObject element = (JsonObject)parser.parse(response);
        	return element.get("success").getAsBoolean();
		}catch(UnknownHostException uhe){
			log("Unknown host exception");
			uhe.printStackTrace();
			return false;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
    }
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		log(result + "");
		if(result) {
			Toast.makeText(activity, "Email sent successfully. You will get your license key on " + email, Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(activity, "Failed to send email!", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void log(String msg){
		Log.e(getClass().getName(), msg);
	}
	
}