package com.suaoc.shopfinder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MainEndWorker extends AsyncTask<String, Integer, String>{

	MainActivity activity = null;
	public MainEndWorker(MainActivity ma) {
		this.activity = ma;
	}

	@Override
	protected String doInBackground(String... params) {
		String response = "";
		InputStream inputStream = null;
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(params[0]);
			
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputStream = httpEntity.getContent();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			while((line = br.readLine()) != null) {
				// log(line);
				response += line;
			}
			
	        JsonParser parser = new JsonParser();
        	JsonObject element = (JsonObject)parser.parse(response);
        	return element.get("mainEnd").getAsString();
		}catch(UnknownHostException uhe){
			log("Unknown host exception");
			uhe.printStackTrace();
			return null;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
    }
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		activity.processMainEnd(result);
	}
	
	public void log(String msg){
		Log.e(getClass().getName(), msg);
	}
	
}