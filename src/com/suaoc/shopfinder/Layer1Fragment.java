package com.suaoc.shopfinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class Layer1Fragment extends Fragment {

	Layer1Adapter adapter = null;
	GridView gv = null;
	ProgressBar pb = null;
	BgImageWorker bgw = null;
	Layer1Worker l1w = null;
	boolean isUpdating = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.grid_layer1, container, false);
		pb = (ProgressBar) v.findViewById(R.id.layer1prg);

		Button goOnBtn = (Button) v.findViewById(R.id.goBtn);
		goOnBtn.setOnClickListener(goBtnListener);
		return v;
	}
	
	public void showProgressBar() {
		if(pb != null) {
			pb.setVisibility(View.VISIBLE);
		}
		if(gv != null) {
			gv.setVisibility(View.INVISIBLE);
		}
	}
	
	public void hideProgressBar() {
		if(pb != null) {
			pb.setVisibility(View.GONE);
		}
		if(gv != null) {
			gv.setVisibility(View.VISIBLE);
		}
	}

	public void start(boolean local, boolean updating) {
		pb.setVisibility(View.VISIBLE);
		this.isUpdating = updating;
		getView().findViewById(R.id.goBtn).setEnabled(false);
		
		ImageLoader il = new ImageLoader(Constants.getFilesDirectory(getActivity()));
		if(il.exists(Constants.URLs.bgImageURL)) {
			ImageView iv = (ImageView) getActivity().findViewById(R.id.bg_image);
			iv.setBackgroundDrawable(new BitmapDrawable(il.getImage(Constants.URLs.bgImageURL)));
		} else {
			bgw = (BgImageWorker) new BgImageWorker(this).execute(Constants.URLs.bgImageURL);
		}
		l1w = new Layer1Worker(this);
		if (local) {
			// load data from local database;
			String data = ((MainActivity) getActivity()).getLayer1();
			if (data == null) {
				// we have valid mainEnd date locally but we don't have valid local data
				l1w.start(false, null, Constants.URLs.layer1URL);
			} else {
				log("Loading from local database");
				l1w.start(true, data, Constants.URLs.layer1URL);
			}
		} else {
			l1w.start(false, null, Constants.URLs.layer1URL);
		}
	}

	public void setBgImage(Bitmap b) {
		if (l1w != null && l1w.getStatus() == AsyncTask.Status.FINISHED) {
			ImageView iv = (ImageView) getActivity().findViewById(R.id.bg_image);
			iv.setBackgroundDrawable(new BitmapDrawable(b));
			ImageLoader il = new ImageLoader(Constants.getFilesDirectory(getActivity()));
			il.saveToInternalSorage(bgw.url, b);
			l1w = null;
			bgw = null;
		}
	}

	public void updateGridView(final List<Layer1Object> objs, boolean error) {
		MainActivity ma = (MainActivity) getActivity();
		if(ma == null || getActivity().isFinishing()) {
			return;
		}
		if(error && ma.getLayer1() == null) {
			// we don't have layer 1 data locally and can't connect to internet
			ma.showEnableInternetDialog(false);
			return;
		}

		if (bgw != null && bgw.getStatus() == AsyncTask.Status.FINISHED) {
			ImageView iv = (ImageView) getActivity().findViewById(R.id.bg_image);
			iv.setBackgroundDrawable(new BitmapDrawable(bgw.bgImage));
			ImageLoader il = new ImageLoader(Constants.getFilesDirectory(getActivity()));
			il.saveToInternalSorage(bgw.url, bgw.bgImage);
			bgw = null;
			l1w = null;
		}
		pb.setVisibility(View.GONE);
		getView().findViewById(R.id.goBtn).setEnabled(true);
		if (objs.size() <= 0) {
			return;
		}
		gv = (GridView) getView().findViewById(R.id.grid_view);
		gv.setVisibility(View.VISIBLE);
		adapter = new Layer1Adapter(getActivity(), objs);
		gv.setAdapter(adapter);
		gv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				((CustomTextView) ((RelativeLayout) v).findViewById(R.id.name)).toggle(objs.get(position).isSelected, false);
				objs.get(position).isSelected = !(objs.get(position).isSelected);
				// Toast.makeText(getActivity(), "selected " + position, Toast.LENGTH_SHORT).show();
			}
		});
		if(isUpdating) {
			Layer2ORM.clearAll(getActivity());
			// show update successful
			showUpdateSuccessfulDialog();
		}
	}

	private OnClickListener goBtnListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if(adapter == null) {
				return;
			}
			// if we have entries in local database for selected items, show layer 2 for those items
			ArrayList<String> cats = Layer2ORM.getLocalSavedLevelh(getActivity().getApplicationContext());
			log(cats.toString());
			boolean update = false;
			ArrayList<String> filters = new ArrayList<String>();
			for (Layer1Object o : adapter.layer1) {
				if (!o.isSelected) {
					continue;
				}
				filters.add(o.name);
				if(!cats.contains(o.name) /*&& !visited.contains(o.name)*/) {
					update = true;
					log(o.name);
				}
			}
			if(filters.size() == 0) {
				Toast.makeText(getActivity(), "Please select at least one item", Toast.LENGTH_SHORT).show();
				return;
			}
			if(cats.size() == 0) {
				// download
				showDataDownloadDialog(filters);
			} else if(update) {
				// update
				showDataUpdateDialog(filters);
			} else {
				// only show
				((MainActivity) getActivity()).showLayer2Fragment(true, filters);
			}
		}
	};

	private void showUpdateSuccessfulDialog() {
		final Dialog ld = new Dialog(getActivity());
		ld.requestWindowFeature(Window.FEATURE_NO_TITLE);
		ld.setContentView(R.layout.dialog_database_update);
		((TextView) ld.findViewById(R.id.msg)).setText("The app has successfully update your local database.");
		ld.setCancelable(false);
		((Button) ld.findViewById(R.id.btnok)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ld.dismiss();
			}
		});
		ld.show();
	}
	
	public void onExit() {
		/*	if(adapter != null)
			adapter.clearSelected();
			*/
	}

	public void showDataDownloadDialog(final ArrayList<String> filters) {
		MainActivity ma = (MainActivity) getActivity();
		if(!ma.isNetworkConnected()) {
			ma.showEnableInternetDialog(true);
			return;
		}
		final Dialog d = new Dialog(getActivity());
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_data_download);
		d.setCancelable(false);
		Button ok = (Button) d.findViewById(R.id.btnok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// download layer 2 for selected items
				d.dismiss();
				((MainActivity) getActivity()).showLayer2Fragment(false, filters);
			}
		});
		d.show();
	}

	public void showDataUpdateDialog(final ArrayList<String> filters) {
		MainActivity ma = (MainActivity) getActivity();
		if(!ma.isNetworkConnected()) {
			ma.showEnableInternetDialog(true);
			return;
		}
		final Dialog d = new Dialog(getActivity());
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_data_update);
		d.setCancelable(false);
		Button ok = (Button) d.findViewById(R.id.btnok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// update layer 2 for selected items
				d.dismiss();
				((MainActivity) getActivity()).showLayer2Fragment(false, filters);
			}
		});
		d.show();
	}

	@Override
	public void onDestroy() {
		Gson g = new Gson();

		if (adapter != null) {
			log("Saving layer1");
			((MainActivity) getActivity()).saveLayer1(g.toJson(adapter.getData()));
		}
		super.onStop();
	}

	public void log(String msg) {
		Log.e(getClass().getName(), msg + "");
	}

	static class ViewHolder {
		public CustomTextView nameView = null;
	}

	public class Layer1Adapter extends ArrayAdapter<Layer1Object> {
		private Activity context = null;
		public List<Layer1Object> layer1 = null;

		public Layer1Adapter(Activity context, List<Layer1Object> objs) {
			super(context, R.layout.grid_item_layer1);
			this.context = context;
			this.layer1 = objs;
			Collections.sort(layer1, new Comparator<Layer1Object>() {

				@Override
				public int compare(Layer1Object lhs, Layer1Object rhs) {
					return lhs.name.compareTo(rhs.name);
				}
			});
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;
			ViewHolder holder;
			// reuse row view, if possible
			if (rowView == null) {
				LayoutInflater inflater = context.getLayoutInflater();
				rowView = inflater.inflate(R.layout.grid_item_layer1, null);
				holder = new ViewHolder();
				holder.nameView = (CustomTextView) rowView.findViewById(R.id.name);
				rowView.setTag(holder);
			}
			// set title and image url
			holder = (ViewHolder) rowView.getTag();
			final Layer1Object lo = layer1.get(position);
			holder.nameView.setBg(lo.isSelected, false);
			holder.nameView.setText(lo.name);

			return rowView;
		}

		@Override
		public int getCount() {
			return this.layer1.size();
		}

		@Override
		public Layer1Object getItem(int position) {
			return this.layer1.get(position);
		}

		public List<Layer1Object> getData() {
			return this.layer1;
		}
		
		public void clearSelected() {
			for (Layer1Object o: layer1) {
				o.isSelected = false;
			}
		}
	}
}
