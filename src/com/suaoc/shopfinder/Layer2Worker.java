package com.suaoc.shopfinder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Layer2Worker extends AsyncTask<String, Integer, Boolean>{

	protected Layer2Fragment ml = null;
	List<Layer2Object> layer2 = new ArrayList<Layer2Object>();
	boolean local = false;
	ArrayList<String> filters = null;
	Handler handler = new Handler();
	int w = 0, h = 0;
    File directory = null;

	public Layer2Worker(Layer2Fragment ml, boolean l, ArrayList<String> filters) {
		this.ml = ml;
		this.local = l;
		this.filters = filters;
		DisplayMetrics dm = ml.getActivity().getResources().getDisplayMetrics();
		w = dm.widthPixels;
		h = dm.heightPixels;
		ContextWrapper cw = new ContextWrapper(ml.getActivity().getApplicationContext());
		directory = cw.getDir(Constants.IMAGES_FOLDER, Context.MODE_PRIVATE);
	}
	
	private void parse(String response) {
//		 log(response);
        JsonParser parser = new JsonParser();
        JsonArray objects = null;
    	JsonObject element = (JsonObject)parser.parse(response);
    	objects = (JsonArray) element.get("objects");
        final GsonBuilder gsonBuilder = new GsonBuilder();
        final Gson gson = gsonBuilder.create();

        for (String s : filters) {
        	Layer2Object o = new Layer2Object();
        	o.ServerId = -1;
        	o.Levelh = s;
        	Layer2ORM.insert(ml.getActivity().getApplicationContext(), o);
		}
        
        log(objects.size() + "");
    	for(int i=0; i < objects.size(); i++) {
			Layer2Object o = gson.fromJson(objects.get(i), Layer2Object.class);
			o.Detailtext2 = o.Detailtext2.replace("\\", "\n");
			
    		o.sortField = o.Name2;
    		if(o.Name2.length() < 1) {
    			o.sortField = o.Brand2;
    		}
    		ImageLoader il = new ImageLoader(directory);
    		setImageUrl(o);
    		if(o.PictInUse.length() > 0) {
    			log("requesting image: " + o.PictInUse);
    			il.requestImage(o.PictInUse);
    		}
			layer2.add(o);
			Layer2ORM.insert(ml.getActivity().getApplicationContext(), o);
		}
    	((MainActivity) ml.getActivity()).setLastUpdate();
	}

	@Override
	protected Boolean doInBackground(String... args) {
		if(local) {
			layer2 = Layer2ORM.getAll(ml.getActivity().getApplicationContext(), filters);
			return true;
		}
		try {
			String q = "";
			for (String s : filters) {
				q += "&Levelh__in=" + URLEncoder.encode(s);
			}
			String u = Constants.URLs.layer2URL + q; 
			log("connecting to " + u);
			parse(getDataFromServer(u));
		}catch(UnknownHostException uhe){
			log("Unknown host exception");
			uhe.printStackTrace();
			return false;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
        return true;
    }
	
	private String getDataFromServer(String url) throws ClientProtocolException, IOException {
		InputStream inputStream = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		
		HttpResponse httpResponse = httpClient.execute(httpGet);
		HttpEntity httpEntity = httpResponse.getEntity();
		inputStream = httpEntity.getContent();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;
		String response = "";
		while((line = br.readLine()) != null) {
			// log(line);
			response += line;
		}
		br.close();
		inputStream.close();
		return response;
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if(ml.getActivity().isFinishing()) {
			return;
		}
		if(!result) {
			// failed to get the json data
			Toast.makeText(ml.getActivity(), "An error occurred. Please try later with internet connection!", Toast.LENGTH_LONG).show();
			ml.updateGridView(layer2);
			return;
		}
		ml.updateGridView(layer2);
		if(local) {
			return;
		}
    	r = new Runnable() {
			
			@Override
			public void run() {
		    	for (Layer2Object o : layer2) {
		    		o.sortField = o.Name2;
		    		if(o.Name2.length() < 1) {
		    			o.sortField = o.Brand2;
		    		}
		    		ImageLoader il = new ImageLoader(directory);
		    		setImageUrl(o);
		    		if(o.PictInUse.length() > 0) {
		    			log("requesting image: " + o.PictInUse);
		    			il.requestImage(o.PictInUse);
		    		}
					Layer2ORM.update(ml.getActivity(), o);
				}
			}
		};
		// handler.post(r);
	}
	
	private void setImageUrl(Layer2Object o) {
		o.PictInUse = o.Detailpict2xl;
		if(w<=320 && h<=480){
			// xss
			o.PictInUse = o.Detailpict2xxs;
		} else if(w<=540 && h<=960){
			// xs
			o.PictInUse = o.Detailpict2xs;
		} else if(w<=800 && h<=1280){
			// s
			o.PictInUse = o.Detailpict2s;
		} else if(w<=1080 && h<=19200){
			// m
			o.PictInUse = o.Detailpict2m;
		} else if(w<=1600 && h<=2540){
			// l
			o.PictInUse = o.Detailpict2l;
		}
	}

	public void cancel() {
		if(r != null) {
			handler.removeCallbacks(r);
		}
	}
	
	public void log(String msg){
		Log.e(getClass().getName(), msg);
	}
	Runnable r = null;
}