package com.suaoc.shopfinder;

import com.google.gson.annotations.SerializedName;

public class Layer1Object {

	@SerializedName("PDFURL1")
	public String pdfURL = "";
	
	@SerializedName("aboutText1")
	public String aboutText1 = "";
	
	@SerializedName("articleNumber")
	public String articleNumber = "";
	
	@SerializedName("facebookURL")
	public String facebookURL = "";
	
	@SerializedName("googlePlusURL")
	public String googlePlusURL = "";
	
	@SerializedName("logo")
	public String logo = "";
	
	@SerializedName("mainEnd")
	public String mainEnd = "";
	
	@SerializedName("name1")
	public String name = "";
	
	@SerializedName("twitterURL")
	public String twitterURL = "";
	
	public boolean isSelected = false;
	
/*	public static class ObjDeserializer implements JsonDeserializer<Layer1Object> {

		@Override
		public Layer1Object deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context) throws JsonParseException {
			final JsonObject jsonObject = json.getAsJsonObject();
			
			final Layer1Object obj = new Layer1Object();
			obj.title = jsonObject.get("headline").getAsString();
			Type type = new TypeToken<ArrayList<KeyArt>>() {}.getType();
			ArrayList<KeyArt> ka = context.deserialize(jsonObject.get("keyArtImages"), type);
			obj.keyArt = ka.get(0);
			return obj;
		}
	}
*/
}
