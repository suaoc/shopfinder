package com.suaoc.shopfinder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImageLoader {

	private ExecutorService executorService;
	private final int NUM_THREADS = 5;
	private File imagesDirectory = null;

	public ImageLoader(File directory) {
		imagesDirectory = directory;
		executorService = Executors.newFixedThreadPool(NUM_THREADS);
	}

	/**
	 * Get and display image from cache, if its there, otherwise start <code>ImageFetcher</code> to fetch from url resource
	 * 
	 * @param url
	 * @param imageView
	 */
	public void requestImage(String url ) {
		if(exists(url)){
			return;
		}
		executorService.submit(new ImageFetcher(url));
	}
	
	public Bitmap getImage(String url) {
		String fileName = UUID.nameUUIDFromBytes(url.getBytes()).toString();
		try {
			File f = new File(imagesDirectory, fileName);
			if(!f.exists()) {
				requestImage(url);
			} else {
				return BitmapFactory.decodeStream(new FileInputStream(f));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String saveToInternalSorage(String url, Bitmap bitmapImage){
        // Create imageDir
		String fileName = UUID.nameUUIDFromBytes(url.getBytes()).toString();
        File d = new File(imagesDirectory, fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(d);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagesDirectory.getAbsolutePath();
    }

	public boolean exists(String url) {
        // Create imageDir
        File d = new File(imagesDirectory, UUID.nameUUIDFromBytes(url.getBytes()).toString());
        return d.exists();
	}
	/**
	 * Fetch bitmap from url resource, and display it in the imageview
	 * 
	 * @author aoc
	 * 
	 */
	private class ImageFetcher implements Runnable {
		String url = null;

		ImageFetcher(String url) {
			this.url = url;
		}

		@Override
		public void run() {
			try {
				// fetch image
				InputStream in = new URL(url).openStream();
				final Bitmap bmp = BitmapFactory.decodeStream(in);
				// the imageview hasn't been re-used, so display bitmap
				saveToInternalSorage(url, bmp);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
