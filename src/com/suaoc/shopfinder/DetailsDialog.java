package com.suaoc.shopfinder;

import java.io.File;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class DetailsDialog extends Dialog {

	ImageView iv = null;
	TextView tv = null;
	
	public DetailsDialog(Context context) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_details);
		iv = (ImageView) findViewById(R.id.detailPic);
		tv = (TextView) findViewById(R.id.detailText);
		iv.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(tv.getText().length() <= 0) {
					// no text view
					return;
				}
				if(tv.getVisibility() == View.VISIBLE) {
					tv.setVisibility(View.GONE);
				} else {
					tv.setVisibility(View.VISIBLE);
				}
			}
		});
		tv.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(iv.getTag() == null) {
					// no image
					return;
				}
				if(iv.getVisibility() == View.VISIBLE) {
					LayoutParams lp = (LayoutParams) tv.getLayoutParams();
					lp.height = iv.getHeight();
					iv.setVisibility(View.GONE);
					tv.setLayoutParams(lp);
					tv.setMaxLines(17);
					tv.invalidate();
				} else {
					LayoutParams lp = (LayoutParams) tv.getLayoutParams();
					lp.height = LayoutParams.WRAP_CONTENT;
					tv.setLayoutParams(lp);
					String t = (String) tv.getText();
					tv.setText("");
					tv.setMaxLines(4);
					tv.setText(t);
					tv.invalidate();
					iv.setVisibility(View.VISIBLE);
				}
			}
		});
	}
	
	public void start(String picURL, String text) {
		if(text.length() <= 0) {
			tv.setVisibility(View.GONE);
		} else {
			tv.setText(text);
		}
		if(picURL.length() <= 0) {
			// no url, only show text
			iv.setVisibility(View.GONE);
			tv.setMaxLines(17);
			tv.setText(text);
		} else {
	        ContextWrapper cw = new ContextWrapper(getContext());
	        File directory = cw.getDir(Constants.IMAGES_FOLDER, Context.MODE_PRIVATE);
			ImageLoader il = new ImageLoader(directory);
			Bitmap b = il.getImage(picURL);
			if(b != null) {
				iv.setTag("bitmap");
				int w = getContext().getResources().getDisplayMetrics().widthPixels;
				if(b.getWidth() > w) {
					b = ThumbnailUtils.extractThumbnail(b, w, w);
				}
				getWindow().setLayout(b.getWidth(), LayoutParams.WRAP_CONTENT);
				iv.setImageBitmap(b);
			} else {
				iv.setVisibility(View.GONE);
			}
		}
		show();
	}

}
