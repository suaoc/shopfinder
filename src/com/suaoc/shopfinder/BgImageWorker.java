package com.suaoc.shopfinder;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class BgImageWorker extends AsyncTask<String, Integer, Bitmap>{

	Layer1Fragment l1Fragment = null;
	String url = null;
	
	public BgImageWorker(Layer1Fragment ma) {
		this.l1Fragment = ma;
	}

	@Override
	protected Bitmap doInBackground(String... params) {
		String response = "";
		InputStream inputStream = null;
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(url = params[0]);
			
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputStream = httpEntity.getContent();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			while((line = br.readLine()) != null) {
				// log(line);
				response += line;
			}
	        JsonParser parser = new JsonParser();
        	JsonObject element = (JsonObject)parser.parse(response);
        	String bgImageUrl = element.get("bgPic").getAsString();
        	return getRemoteImage(new URL(bgImageUrl)); 
		}catch(UnknownHostException uhe){
			log("Unknown host exception");
			uhe.printStackTrace();
			return null;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
    }
	
	public Bitmap getRemoteImage(final URL aURL) {
	    try {
	        final URLConnection conn = aURL.openConnection();
	        conn.connect();
	        final BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
	        final Bitmap bm = BitmapFactory.decodeStream(bis);
	        bis.close();
	        return bm;
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	    return null;
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}
	
	public Bitmap bgImage = null;
	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
		if(result != null) {
			bgImage = result;
			l1Fragment.setBgImage(result);
		} else {
			log("Bitmap image is null");
		}
	}
	
	public void log(String msg){
		Log.e(getClass().getName(), msg);
	}
	
}