package com.suaoc.shopfinder;

import java.util.Formatter;
import java.util.Locale;

import com.google.gson.annotations.SerializedName;

public class Layer2Object {

	@SerializedName("id")
	public long ServerId = -1;
	
	@SerializedName("Addition")
	public String Addition = "";

	@SerializedName("Artnr2")
	public String Artnr2 = "";

	@SerializedName("Bp2")
	public String Bp2 = "";

	@SerializedName("Brand2")
	public String Brand2 = "";

	@SerializedName("Cat2")
	public String Cat2 = "";

	@SerializedName("Concurrent")
	public String Concurrent = "";

	@SerializedName("Dataend2")
	public String Dataend2 = "";

	@SerializedName("Detailpict2l")
	public String Detailpict2l = "";

	@SerializedName("Detailpict2m")
	public String Detailpict2m = "";

	@SerializedName("Detailpict2s")
	public String Detailpict2s = "";

	@SerializedName("Detailpict2xl")
	public String Detailpict2xl = "";

	@SerializedName("Detailpict2xs")
	public String Detailpict2xs = "";

	@SerializedName("Detailpict2xxl")
	public String Detailpict2xxl = "";

	@SerializedName("Detailpict2xxs")
	public String Detailpict2xxs = "";

	@SerializedName("Detailtext2")
	public String Detailtext2 = "";

	@SerializedName("Filter1")
	public String Filter1 = "";

	@SerializedName("Included")
	public String Included = "";

	@SerializedName("Levelh")
	public String Levelh = "";

	@SerializedName("Name2")
	public String Name2 = "";

	@SerializedName("Pdfurl2")
	public String Pdfurl2 = "";

	@SerializedName("Price2")
	public String Price2 = "";

	@SerializedName("Priceend3")
	public String Priceend3 = "";

	@SerializedName("Pricenew2")
	public String Pricenew2 = "";

	@SerializedName("Pu2")
	public String Pu2 = "";

	@SerializedName("Saving2")
	public String Saving2 = "";

	@SerializedName("Special2")
	public String Special2 = "";

	@SerializedName("Stock2")
	public String Stock2 = "";

	@SerializedName("Stockend2")
	public String Stockend2 = "";
	
	@SerializedName("curled")
	public boolean curled = false;

	public String PictInUse = "";
	public boolean isSelected = false;
	public String sortField = "";
	
	public String strFormat() {
		String finalResult = String.format("%-30s  %-30s  %-30s  %-10s  %-10s\n", Name2, Brand2, Artnr2, Stock2, Price2.equals("0") ? "" : Price2);

/*		StringBuffer sb = new StringBuffer();
		Formatter f = new Formatter(sb, Locale.getDefault());
		f.format(" %-50s \t", Name2);
		f.format(" %-50s \t", Brand2);
		f.format(" %-50s \t", Artnr2);
		String finalResult = sb.toString() + "\n";
*/
		System.out.println(finalResult);
		return finalResult;
	}
	public static String formatHead() {
		/*
		Name			  		Brand	 		art-nr.	   		stock 	price
		---------------------------------------------------------------------------------------------------------------------------------------------
		Kadett 					Opel			496409345332 		400	€ 350,00
		Ford Capri							454522566 		100    € 1.150,00*/
		return String.format("%-30s %-30s %-30s %-10s %-10s\n", "Name", "Brand", "art-nr", "stock", "price") +
				"---------------------------------------------------------------------------------------------------------------------------------------------\n";
	}
}
