package com.suaoc.shopfinder;

import java.io.File;
import java.util.HashMap;

import android.content.Context;
import android.content.ContextWrapper;

public class Constants {

	/*** Change these to needs ***/
	public static final boolean IS_PLAY_STORE_VERSION = false; // change this to true for store version and to false for personal version 
	
	public static final int TRIAL_DAYS_STORE = 30; // number of trial days for store license 

	public static final int TRIAL_DAYS_PERSONAL = 14; // number of trial days for personal license
	
	public static final int UPDATE_INTERVAL = 10; // check for updates after 10 minutes
	
	public static final String SERVER_URL = "https://findshop.herokuapp.com";
	/***************************/
	
	/*********** following don't need any modifications *******************/
	public static final String IMAGES_FOLDER = "images";
	public static boolean LOG = true;
	public static final int LICENSE_DAYS = 0;

/*	0503002  2 weeks
	0503004  1 month
	0503008  2 months
	0503012  3 months 
	0503024  6 months
	0503052  1 year
	0503000  unlimited
*/
	public static int isMasterKey(String key) {
		HashMap<String, Integer> codes = new HashMap<String, Integer>();
		codes.put("0503002",  14);
		codes.put("0503004",  30);
		codes.put("0503008",  60);
		codes.put("0503012",  90);
		codes.put("0503024",  180);
		codes.put("0503052",  365);
		codes.put("0503000",  -1);
		if(codes.containsKey(key)) {
			return codes.get(key);
		}
		return 0;
	}
	
	public static class URLs {
		public static final String baseURL = SERVER_URL;
		public static final String layer1URL = baseURL + "/api/v1/layer1/?format=json";
		public static final String layer2URL = baseURL + "/api/v1/layer2/?format=json";
		public static final String mainEndURL = baseURL + "/api/v1/mainend/1/?format=json";
		public static final String bgImageURL = baseURL + "/api/v1/background/1/?format=json";
		public static final String emailURL = baseURL + "/email/";
	}
	
	public static class Keys {
		public static final String MAIN_END = "MAIN_END";
		public static final String REGISTERED = "REGISTERED";
		public static final String LAYER1_JSON = "LAYER1_JSON";
		
		public static final String NAME = "name";
		public static final String COMPANY = "company";
		public static final String EMAIL = "email";
		public static final String LICENSE_CODE = "license_code";
		public static final String LICENSE_KEY = "license_key";
		public static final String LICENSE_DURATION = "ld";
		public static final String ZERO_CATEGORIES = "ZERO_CATEGORIES";
		public static final String LAST_UPDATED = "LAST_UPDATED";
		public static final String FIRST_USE = "FIRST_USE";
	}
	
	public static File getFilesDirectory(Context c) {
		ContextWrapper cw = new ContextWrapper(c);
		return cw.getDir(Constants.IMAGES_FOLDER, Context.MODE_PRIVATE);
	}
}
