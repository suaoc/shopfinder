package com.suaoc.shopfinder;

import java.io.UnsupportedEncodingException;

import android.util.Base64;

public class Utils {

	public static String encode(String enc) {
		byte[] data = null;
		try {
			data = enc.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		return Base64.encodeToString(data, Base64.DEFAULT);
	}

	public static String decode(String b) {
		byte[] data1 = Base64.decode(b, Base64.DEFAULT);
		String dec = null;
		try {
			dec = new String(data1, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return dec;
	}
}
