package com.suaoc.shopfinder;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


public class LicenseToolActivity extends Activity {

	Spinner spinner = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_license_tool);
		init();

		spinner = (Spinner) findViewById(R.id.spinner1);
		Spinner version = (Spinner) findViewById(R.id.version);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.license_duration, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);

		ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.versions, android.R.layout.simple_spinner_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		version.setAdapter(adapter2);
	}

	public void init() {
		final EditText fieldName = (EditText) findViewById(R.id.fieldName);
		final EditText fieldCompany = (EditText) findViewById(R.id.fieldCompany);
		final EditText fieldCode = (EditText) findViewById(R.id.fieldCode);
		final EditText fieldKey = (EditText) findViewById(R.id.fieldKey);
		final EditText fieldEmail = (EditText) findViewById(R.id.fieldEmail);

		Button generate = (Button) findViewById(R.id.btngenerate);
		generate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String fieldCodeStr = fieldCode.getText().toString();

				if (fieldCodeStr == null || fieldCodeStr.length() <= 0) {
					Toast.makeText(LicenseToolActivity.this, "Please enter code", Toast.LENGTH_SHORT).show();
					return;
				}
				SimpleDateFormat format = new SimpleDateFormat("ddMMHH");
				Calendar c = Calendar.getInstance();
				Date now = new Date();
				c.setTime(now);
				c.add(Calendar.DAY_OF_MONTH, 1);
				String t = format.format(c.getTime());
				String key = t +  ":" + fieldCodeStr + ":" + getResources().getIntArray(R.array.license_duration_days)[spinner.getSelectedItemPosition()];//UUID.nameUUIDFromBytes((fieldCodeStr).getBytes()).toString().replace("-", "").substring(0, 11).toUpperCase();
				String e = encode(key);
				log(key + " > " + e);
				fieldKey.setText(e);
				log(e + " > " + decode(e));
			}
		});
		// send button handler
		Button send = (Button) findViewById(R.id.btnsend);
		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final String fieldNameStr = fieldName.getText().toString();
				final String fieldCompanyStr = fieldCompany.getText().toString();
				final String fieldCodeStr = fieldCode.getText().toString();
				final String fieldKeyStr = fieldKey.getText().toString();
				final String fieldEmailStr = fieldEmail.getText().toString();

				if (fieldKeyStr == null || fieldKeyStr.length() <= 0 || fieldEmailStr == null || fieldEmailStr.length() <= 0) {
					Toast.makeText(LicenseToolActivity.this, "Please generate key first and enter email address", Toast.LENGTH_SHORT).show();
					return;
				}
				if (!isNetworkConnected()) {
					showEnableInternetDialog();
					return;
				}
				Toast.makeText(getApplicationContext(), "Sending..", Toast.LENGTH_SHORT).show();
				EmailWorker ew = new EmailWorker((Activity) LicenseToolActivity.this, fieldNameStr + "," + fieldCompanyStr + "," + fieldCodeStr + "," + fieldKeyStr, fieldEmailStr);
				ew.execute(Constants.URLs.emailURL + "?key=1");
			}
		});

	}

	// check network connectivity
	public boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() == null) {
			// There are no active networks.
			return false;
		}
		return true;
	}

	int SETTINGS_SCREEN_CODE = 0;

	public void showEnableInternetDialog() {
		final Dialog d = new Dialog(this);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_enable_internet);
		d.setCancelable(false);
		Button ok = (Button) d.findViewById(R.id.btnok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS), SETTINGS_SCREEN_CODE);
				d.dismiss();
			}
		});
		d.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SETTINGS_SCREEN_CODE) {
			// we are back from settings screen
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e) {
					}
					runOnUiThread(new Runnable() {
						public void run() {
							try {
								if (!isNetworkConnected()) {
									showNoInternetDialog();
								}
							} catch (Exception e) {
							}
						}
					});
				}
			}).start();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	// show not internet dialog
	public void showNoInternetDialog() {
		final Dialog d = new Dialog(this);
		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.dialog_no_internet);
		d.setCancelable(false);
		Button ok = (Button) d.findViewById(R.id.btnExit);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// no internet, so close activity
				d.dismiss();
				finish();
			}
		});
		d.show();
	}

	// encode information
	public static String encode(String enc) {
		byte[] data = null;
		try {
			data = enc.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		return Base64.encodeToString(data, Base64.DEFAULT);
	}

	// decode license information
	public static String decode(String b) {
		byte[] data1 = Base64.decode(b, Base64.DEFAULT);
		String dec = null;
		try {
			dec = new String(data1, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return dec;
	}
	
	public void log(String msg){
		Log.e(getClass().getName(), msg);
	}
	
	public static void disableSoftInputFromAppearing(EditText editText) {
	    if (Build.VERSION.SDK_INT >= 11) {
	        editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
	        editText.setTextIsSelectable(true);
	    } else {
	        editText.setRawInputType(InputType.TYPE_NULL);
	        editText.setFocusable(true);
	    }
	}
}
